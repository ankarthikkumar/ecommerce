
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class=" nav-item"><a href="{{URL('/')}}/"><i class="la la-dashboard"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/category"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Category</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/size"><i class="icon-user"></i><span class="menu-title" data-i18n="nav.dash.main">Size</span></a>
        </li> 
        <li class=" nav-item"><a href="{{URL('/')}}/collection"><i class="icon-user"></i><span class="menu-title" data-i18n="nav.dash.main">Collection</span></a>
        </li><!--
        <li class=" nav-item"><a href="{{URL('/')}}/admin/clients/3"><i class="icon-users"></i><span class="menu-title" data-i18n="nav.dash.main">Manage Users</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/application"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Discussion Forum</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/jobs"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Current Jobs</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/completed_jobs"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Completed Jobs</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/report"><i class="la la-newspaper-o"></i><span class="menu-title" data-i18n="nav.dash.main">Manage Reports</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/promocode"><i class="la la-ticket"></i><span class="menu-title" data-i18n="nav.dash.main">Promo code</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/rating"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Rating & Reviews</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/send_mail"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Send Email</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/email_configuration"><i class="la la-comments"></i><span class="menu-title" data-i18n="nav.dash.main">Email Configuration</span></a>
        </li>
        <li class=" nav-item"><a href="{{URL('/')}}/admin/commission"><i class="la la-cogs"></i><span class="menu-title" data-i18n="nav.dash.main">Commission Settings</span></a>

        <li class=" nav-item"><a href="{{URL('/')}}/admin/notification"><i class="la la-commenting-o"></i><span class="menu-title" data-i18n="nav.dash.main">Notification</span></a>
        </li>

        <li class=" nav-item"><a href="{{URL('/')}}/admin/payout"><i class="la la-cogs"></i><span class="menu-title" data-i18n="nav.dash.main">Service Provider payouts</span></a>
        
        
        <li class=" nav-item"><a href="{{URL('/')}}/admin/settings"><i class="la la-cogs"></i><span class="menu-title" data-i18n="nav.dash.main">Settings</span></a></li> -->
        
      </ul>        
    </div>