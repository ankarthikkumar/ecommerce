
@extends('layout.adminmaster')

@section('title')

COM - Ultimate Freelance Marketplace
@endsection

@section('content')
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Jobs</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item active">Jobs List
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Basic form layout section start -->


        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-head">
                  <div class="card-header">
                   
                  <h4 class="card-title">Project Details</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                  <div class="heading-elements">
                     <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      </ul>
                      </div>
                      
                    </div>
                </div>


<div class="col-sm-12 text-center text-md-left">
                  <div class="row">
                    <div class="col-md-12">
                      <table class="table table-borderless table-sm center">
                        <tbody>
                          <tr>
                            <td><h4>Job Title:</h4></td>
                            <td class="text-left">Designing</td>
                          </tr>
                          <tr>
                            <td><h4>Job Description:</h4></td>
                            <td class="text-left">It can be used for any type of web applications: Project Management, eCommerce backends, CRM, Analytics, Fitness or any custom admin panels. Powered with HTML 5, SASS, GRUNT, Pug & with Bootstrap 4 Stable version. It comes with starter kit which will help developers to get started quickly.</td>
                          </tr>
                          <tr>
                            <td><h4>Category:<h4></td>
                            <td class="text-left">HTML</td>
                          </tr>
                          <tr>
                            <td><h4>Service Provider Name:<h4></td>
                            <td class="text-left">Vishnu</td>
                          </tr>
                          <tr>
                            <td><h4>Client Name:<h4></td>
                            <td class="text-left">Muniraja</td>
                          </tr>
                          
                          <tr>
                            <td><h4>Project Status:</h4></td>
                            <td class="text-left">50%</td>
                          </tr>
                          <tr>
                            <tr>
                            <td><h4>Price:</h4></td>
                            <td class="text-left">$ 2899</td>
                          </tr>
                          
                          
                        </tbody>
                      </table>
                      <div class="text-center pb-2">
                      <a href="{{URL('/')}}/admin/chat" class="btn btn-lg btn-outline-danger">Chat</a>
                    </div>
                    </div>
                  </div>
                </div>



              </div>
            </div>
          </div>
        </section>

        <!-- // Basic form layout section end -->
      </div>
    </div>
  </div>
  <!-- 
  <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
        target="_blank">PIXINVENT </a>, All rights reserved. </span>
      <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
  </footer>-->
  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
    <script src="{{URL::asset('public/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>

  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/dropdowns/dropdowns.js')}}" type="text/javascript"></script>

  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{URL::asset('public/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
  type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection