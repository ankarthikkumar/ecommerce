
@extends('layout.adminmaster')

@section('title')

COM - Ultimate Freelance Marketplace
@endsection

@section('content')
  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Push Notification</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item active">Push Notification
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <div class="content-body">
        <!-- Revenue, Hit Rate & Deals -->
        <div class="row" style="display: none">
          <div class="col-xl-6 col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Revenue</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
        
<form class="form">
                      <div class="row justify-content-md-center">
                        <div class="col-md-6">
                          <div class="form-body">
                            
                            <div class="form-group">
                              <label for="eventInput4">Push title</label>
                              <input type="email" id="eventInput4" class="form-control" placeholder="Push title" name="push-title">
                            </div>

                            <div class="form-group">
                      <label class="control-label">Select Users</label>
                      <span><input id="onCheckAll" onclick="funMania();" type="checkbox">All</span>
                      <strong id="userCount" style="color: green; margin-left: 5px;">  You have selected <span id="userSize">1</span> users.</strong>
                      <div class="">
                        <select class="form-control" required="" id="allUsers" name="numbers[]" multiple="multiple" style="height:200px;">
                        <option value="1">Saulo Henao (saulo7412@gmail.com)</option>                    
                        <option value="2">Guru Prasanna (mail2guruprasanna3@gmail.com)</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class=" control-label">Enter the Message</label>
                      <div class="">
                        <textarea name="push_message" required="" class="form-control" rows="3"></textarea>
                      </div>
                    </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-actions center">
                        <button type="button" class="btn btn-warning mr-1">
                          <i class="ft-x"></i> Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">
                          <i class="la la-check-square-o"></i> Submit
                        </button>
                      </div>
                    </form>
        <!--/ Total earning & Reports  -->
        <!-- Analytics map based session -->
        
        <!-- Analytics map based session -->
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->

  <!-- BEGIN VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"
  type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/data/jvector/visitor-data.js')}}" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN MODERN JS-->
  <script src="{{URL::asset('public/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/core/app.js')}}" type="text/javascript"></script>
  <script src="{{URL::asset('public/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
  <!-- END MODERN JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{URL::asset('public/app-assets/js/scripts/pages/dashboard-sales.js')}}" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->
</body>
</html>
@endsection