<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
///========get categories==============//////

Route::get('/','Admin\ProductController@index');

Route::get('category','Admin\ProductController@categorylist');

Route::Post('addedit_category','Admin\ProductController@addeditcategory');

Route::get('delete_category/{id}','Admin\ProductController@deletecategory');


//=======Get size====///

Route::get('size','Admin\ProductController@sizelist');

Route::Post('addedit_size','Admin\ProductController@addeditsize');

Route::get('delete_size/{id}','Admin\ProductController@deletesize');

//=======Collection====///

Route::get('colletion','Admin\ProductController@colletionlist');

Route::Post('addedit_collection','Admin\ProductController@addeditcollection');

Route::get('delete_collection/{id}','Admin\ProductController@deletecollection');