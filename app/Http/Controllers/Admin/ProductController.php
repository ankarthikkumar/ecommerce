<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\BaseController;
use DB;

class ProductController extends BaseController
{
    //

    public function index(Request $request){

		$data = $this->users->count();
		return view('admin.index',['data'=>$data]);
	}

    public function categorylist(Request $request){

		$data = $this->categories->get();
		return view('admin.category',['data'=>$data]);
	}

    public function addeditcategory(Request $request){

    	$id=$request->id;
    	if(!$id){
		$data = $this->categories->insert(['category_name'=>$request->category_name,'status'=>1]);
	}else{

		$data = $this->categories->where('id',$id)->update(['category_name'=>$request->category_name,'status'=>1]);
	}

		return back()->with('success','category posted successfully');
	}

    public function deletecategory($id){

		$data = $this->categories->where('id',$id)->delete();
		return back()->with('success','category deleted successfully');
	}

    public function sizelist(Request $request){

		$data = $this->size->get();
		return view('admin.size',['data'=>$data]);
	}

    public function addeditsize(Request $request){

    	$id=$request->id;
    	if(!$id){
		$data = $this->size->insert(['size'=>$request->size_name,'status'=>1]);
	}else{

		$data = $this->size->where('id',$id)->update(['size'=>$request->size_name,'status'=>1]);
	}

		return back()->with('success','size posted successfully');
	}

    public function deletesize($id){

		$data = $this->size->where('id',$id)->delete();
		return back()->with('success','size deleted successfully');
	}


    public function collectionlist(Request $request){

		$data = $this->size->get();
		return view('admin.size',['data'=>$data]);
	}

    public function addeditcollection(Request $request){

    	$id=$request->id;
    	if(!$id){
		$data = $this->size->insert(['size'=>$request->size_name,'status'=>1]);
	}else{

		$data = $this->size->where('id',$id)->update(['size'=>$request->size_name,'status'=>1]);
	}

		return back()->with('success','size posted successfully');
	}

    public function deletecollection($id){

		$data = $this->size->where('id',$id)->delete();
		return back()->with('success','size deleted successfully');
	}

}
